package com.kamal.github

import com.ninjasquad.springmockk.MockkBean
import io.mockk.every
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.*


@WebMvcTest
class HomeControllerTests(@Autowired val mockMvc: MockMvc) {

    @MockkBean
    lateinit var queryRepository: QueryRepository

    @Test
    fun `List queries`() {
        val kotlin1Query = Query("spring", "kotlin", 0, 10, "https://api.github.com/search/repositories?q=spring+language:kotlin&sort=stars&order=desc&page=0&per_page=10")
        val kotlin2Query = Query("spring%20boot", "kotlin", 0, 10, "https://api.github.com/search/repositories?q=spring%20boot+language:kotlin&sort=stars&order=desc&page=0&per_page=10")
        every { queryRepository.findByLang("kotlin") } returns listOf(kotlin1Query, kotlin2Query)
        mockMvc.perform(get("/report/?lang=kotlin").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk)
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("\$.data.[0].query").value(kotlin1Query.query))
                .andExpect(jsonPath("\$.data.[1].query").value(kotlin2Query.query))
    }
}