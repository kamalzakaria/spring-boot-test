package com.kamal.github

import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager
import org.assertj.core.api.Assertions.assertThat


@DataJpaTest
class RepositoriesTests @Autowired constructor(
        val entityManager: TestEntityManager,
        val queryRepository: QueryRepository) {

    @Test
    fun `When findbyLang then return Query`() {
        val query = Query("spring%20boot", "kotlin", 0, 10,"https://api.github.com/search/repositories?q=spring%20boot+language:kotlin&sort=stars&order=desc&page=0&per_page=10")
        entityManager.persist(query)
        entityManager.flush()
        val queries = queryRepository.findByLang("kotlin")
        assertThat(queries).contains(query)
    }
}
