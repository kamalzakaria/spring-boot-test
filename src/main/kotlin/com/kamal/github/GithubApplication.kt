package com.kamal.github

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.web.client.RestTemplate
import java.net.HttpURLConnection
import java.net.URL

@SpringBootApplication
class GithubApplication

fun main(args: Array<String>) {
	runApplication<GithubApplication>(*args)
}
