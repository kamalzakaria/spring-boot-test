package com.kamal.github

import org.springframework.data.repository.CrudRepository

interface QueryRepository : CrudRepository<Query, Long> {
    fun findByLang(lang: String): Iterable<Query>
}