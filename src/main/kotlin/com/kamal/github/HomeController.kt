package com.kamal.github

import org.springframework.web.bind.annotation.*
import java.net.HttpURLConnection
import java.net.URL
import java.time.LocalDateTime
import kotlin.collections.HashMap

@RestController
class HomeController(private val repository: QueryRepository) {

    @GetMapping("/")
    fun home(@RequestParam query: String?,
             @RequestParam lang: String?,
             @RequestParam page: Int?,
             @RequestParam limit: Int?
    ): String {
        val queryStr = query?.replace(" ", "%20") ?: ""
        val urlStr = "https://api.github.com/search/repositories?q=${queryStr}+language:${lang ?: ""}&sort=stars&order=desc&page=${page ?: 0}&per_page=${limit ?: 5}"
        val url = URL(urlStr)

        var response: String = "Hello World"

        with(url.openConnection() as HttpURLConnection) {
            requestMethod = "GET"  // optional default is GET

            println("\nSent 'GET' request to URL : $url; Response Code : $responseCode")

            inputStream.bufferedReader().use {
                it.lines().forEach { line ->
                    println(line)
                    response = line
                }
            }
        }

        val data: Query = Query(query ?: "", lang ?: "", page ?: 0, limit ?: 10, urlStr, LocalDateTime.now())
        println(data)
        repository.save(data)

        return response
    }

    @GetMapping("/report/")
    fun report(@RequestParam lang: String?): HashMap<String, Any> {
        var response: HashMap<String, Any> = HashMap();
        val query = if (lang == null) repository.findAll()
        else repository.findByLang(lang)
        response["total"] = query.count()
        response["data"] = query
        return response
    }
}