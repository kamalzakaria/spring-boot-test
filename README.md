# Spring Boot Test

### Setup

1. Install IntelliJ IDEA IDE
https://www.jetbrains.com/idea/download/#section=windows
2. Clone or download this repo
3. Open project with IntelliJ IDEA
4. Build project 
(click the green hammer icon)


### Run Project
1. Run GithubApplication.kt in /src/main/kotlin/com.kamal.github/ folder.
(right click on the file then select "Run GithubApplicationKt")
The app should be running and listening to port 8090
2. Open your browser then paste this url: 
http://localhost:8090/?query=spring%20boot&lang=kotlin&page=0&limit=10
3. The page should display queries from github in JSON format.
4. You can change the query base on these params:
query: enter any topic you would like to search in github
lang: enter any languange that you prefer, eg: kotlin, java, flutter
page: this is the page number of your query, 1st page is "0"
limit: define how many items per page that you want

### Get Report
1. Run the project
2. Open your browser then paste this url: 
http://localhost:8090/report/
3. This should display all queries made.
4. To get report by language queries, use this url: http://localhost:8090/report/?lang=kotlin
5. The url will search any queries based on kotlin language.
6. You can change the language to other such as java, flutter, etc.


### Unit Test
1. To execute unit test, right click on kotlin folder in /src/test/ folder, then select "Run 'All Tests'"
2. There are 3 tests, and all 3 should pass.